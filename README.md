# Emanuele Peruffo's Komoot Challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

The repository is at [gitlab.com/emanueleperuffo/komoot-challenge](https://gitlab.com/emanueleperuffo/komoot-challenge).

The demo is at [dapper-lollipop-e3754f.netlify.app](https://dapper-lollipop-e3754f.netlify.app/).

## Available Scripts

In the project directory, you can run:

- `yarn start`
- `yarn test`
- `yarn build`
