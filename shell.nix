let
  nixpkgs = fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/4e674086d92e58e691146665397d8be089d38881.tar.gz";
  };

  pkgs = import nixpkgs { config = {}; };

  nodejs = pkgs.nodejs-16_x;
in pkgs.mkShell {
  nativeBuildInputs = [
    # NodeJS
    nodejs
    (pkgs.yarn.override { inherit nodejs; })
  ];
}
