/** @jsxImportSource @emotion/react */
import { css, Global, GlobalProps } from '@emotion/react';
import { History, Location } from 'history';
import { useEffect, useMemo, useState } from 'react';
import { Map } from './components/Map';
import { RouteBuilder } from './components/RouteBuilder';
import { Position } from './types';
import { downloadGpx } from './utils/downloadGpx';
import { parseStateFromPathname } from './utils/parseStateFromPathname';
import { waypoitsToGpx } from './utils/waypointsToGpx';

const breakpoint = '700px';
const desktopMedia = `@media only screen and (min-width: ${breakpoint})`;

const appCss = css({
  display: 'flex',
  position: 'absolute',
  top: 0,
  left: 0,
  bottom: 0,
  right: 0,
  flexDirection: 'column',

  [desktopMedia]: {
    flexDirection: 'row',
  },
});

const globalCss: GlobalProps['styles'] = {
  'html, body': { height: '100%' },
  body: {
    margin: 0,
    fontFamily: [
      'system-ui',
      '-apple-system',
      'BlinkMacSystemFont',
      'Segoe UI',
      'Roboto',
      'Helvetica Neue',
      'sans-serif',
    ],
    WebkitFontSmoothing: 'antialiased',
    MozOsxFontSmoothing: 'grayscale',
  },
};

const routeBuilderCss = css({
  height: '30%',
  overflow: 'auto',

  [desktopMedia]: {
    height: 'auto',
    width: '20rem',
  },
});

const mapCss = css({
  flexGrow: 1,
});

interface Props {
  history: History;
}

export const App: React.FC<Props> = ({ history }) => {
  const [waypoints, setWaypoints] = useState<Position[]>([]);

  const [mapCenterZoom, setMapCenterZoom] = useState<{
    center: Position;
    zoom: number;
  }>();

  useEffect(() => {
    // first time initialization from pathname

    const handleLocationChange = (location: Location) => {
      const { pathname } = history.location;

      const state = parseStateFromPathname(pathname, {
        center: [13.83333, 46.3833318],
        waypoints: [],
        zoom: 14,
      });

      setMapCenterZoom({
        center: state.center,
        zoom: state.zoom,
      });

      setWaypoints(state.waypoints);
    };

    handleLocationChange(history.location);

    const unlisten = history.listen(({ location }) => {
      handleLocationChange(location);
    });

    return () => {
      unlisten();
    };
  }, [history]);

  const center = mapCenterZoom?.center;
  const centerStr = center && center.toString();
  const zoom = mapCenterZoom?.zoom;

  // TODO: find a better way to stringify the waypoints
  // TODO: set max number of waypoints to not have too long URLs
  const waypointsStr = useMemo(() => JSON.stringify(waypoints), [waypoints]);

  useEffect(() => {
    if (!centerStr) {
      return;
    }

    // TODO: extract the formatting of the pathname to a testable function
    history.push(`/c:${centerStr}/z:${zoom}/${waypointsStr}`);
  }, [history, centerStr, zoom, waypointsStr]);

  if (!center || !zoom) {
    return null;
  }

  return (
    <>
      <Global styles={globalCss} />

      <div css={appCss}>
        <div css={routeBuilderCss}>
          <RouteBuilder
            onDownloadRoute={() => {
              downloadGpx(waypoitsToGpx(waypoints));
            }}
            onRemoveWaypoint={(waypointToDelete) => {
              setWaypoints((prevWaypoints) =>
                prevWaypoints.filter(
                  (waypoint) => waypoint !== waypointToDelete,
                ),
              );
            }}
            onSortWaypoints={setWaypoints}
            waypoints={waypoints}
          />
        </div>
        <div css={mapCss}>
          <Map
            center={center}
            zoom={zoom}
            onClick={(waypoint) => {
              setWaypoints((prev) => [...prev, waypoint]);
            }}
            onIdle={({ center: nextCenter, zoom }) => {
              setMapCenterZoom({
                zoom,
                center:
                  nextCenter[0] !== center[0] || nextCenter[1] !== center[1]
                    ? nextCenter
                    : center,
              });
            }}
            waypoints={waypoints}
          />
        </div>
      </div>
    </>
  );
};
