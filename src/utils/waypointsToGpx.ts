import { Position } from '../types';

const namespace = 'http://www.topografix.com/GPX/1/1';

export const waypoitsToGpx = (waypoints: Position[]): string => {
  const xml = document.implementation.createDocument(namespace, null);

  const gpx = xml.appendChild(xml.createElementNS(namespace, 'gpx'));

  gpx.setAttribute('version', '1.1');
  gpx.setAttribute('creator', 'Emanuele Peruffo - Komoot Challenge');

  const rte = gpx.appendChild(xml.createElementNS(namespace, 'rte'));

  for (const [lng, lat] of waypoints) {
    const rtept = rte.appendChild(xml.createElementNS(namespace, 'rtept'));

    rtept.setAttribute('lon', lng.toString());
    rtept.setAttribute('lat', lat.toString());
  }

  return new XMLSerializer().serializeToString(xml);
};
