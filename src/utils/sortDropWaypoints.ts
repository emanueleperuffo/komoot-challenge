import { Position } from '../types';

type Move =
  | { where: 'before'; index: number }
  | { where: 'after'; index: number };

export const sortDropWaypoints = (
  waypoints: Position[],
  droppedIndex: number,
  move: Move,
) =>
  waypoints.flatMap((waypoint, prevIndex) => {
    if (prevIndex === droppedIndex) {
      return [];
    }

    if (prevIndex !== move.index) {
      return [waypoint];
    }

    if (move.where === 'before') {
      return [waypoints[droppedIndex], waypoint];
    }

    return [waypoint, waypoints[droppedIndex]];
  });
