import { sortDropWaypoints } from './sortDropWaypoints';

describe('sortDropWaypoints', () => {
  it('should move before', () => {
    expect(
      sortDropWaypoints(
        [
          [1, 2],
          [3, 4],
          [5, 6],
          [7, 8],
        ],
        1,
        {
          index: 3,
          where: 'before',
        },
      ),
    ).toEqual([
      [1, 2],
      [5, 6],
      [3, 4],
      [7, 8],
    ]);

    expect(
      sortDropWaypoints(
        [
          [1, 2],
          [3, 4],
          [5, 6],
          [7, 8],
        ],
        3,
        {
          index: 0,
          where: 'before',
        },
      ),
    ).toEqual([
      [7, 8],
      [1, 2],
      [3, 4],
      [5, 6],
    ]);
  });

  it('should move after', () => {
    expect(
      sortDropWaypoints(
        [
          [1, 2],
          [3, 4],
          [5, 6],
          [7, 8],
        ],
        1,
        {
          index: 3,
          where: 'after',
        },
      ),
    ).toEqual([
      [1, 2],
      [5, 6],
      [7, 8],
      [3, 4],
    ]);

    expect(
      sortDropWaypoints(
        [
          [1, 2],
          [3, 4],
          [5, 6],
          [7, 8],
        ],
        3,
        {
          index: 0,
          where: 'after',
        },
      ),
    ).toEqual([
      [1, 2],
      [7, 8],
      [3, 4],
      [5, 6],
    ]);
  });
});
