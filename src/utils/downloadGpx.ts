export const downloadGpx = (gpx: string) => {
  const blob = new Blob([gpx], { type: 'application/gpx+xml' });
  const url = URL.createObjectURL(blob);

  const a = document.createElement('a');
  a.href = url;
  a.download = 'Emanuele Peruffo - Komoot Challenge.gpx';
  a.click();

  URL.revokeObjectURL(url);
  a.remove();
};
