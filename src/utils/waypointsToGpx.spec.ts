import { waypoitsToGpx } from './waypointsToGpx';

describe('waypoitsToGpx', () => {
  test('it should generate a valid gpx', () => {
    expect(
      waypoitsToGpx([
        [1, 2],
        [3, 4],
        [5, 6],
      ]),
    ).toMatchInlineSnapshot(
      `"<gpx xmlns=\\"http://www.topografix.com/GPX/1/1\\" version=\\"1.1\\" creator=\\"Emanuele Peruffo - Komoot Challenge\\"><rte><rtept lon=\\"1\\" lat=\\"2\\"/><rtept lon=\\"3\\" lat=\\"4\\"/><rtept lon=\\"5\\" lat=\\"6\\"/></rte></gpx>"`,
    );
  });
});
