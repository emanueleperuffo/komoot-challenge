import { parseStateFromPathname } from './parseStateFromPathname';

describe('parseStateFromPathname', () => {
  test('it should return default if the path is empty', () => {
    expect(
      parseStateFromPathname('/', {
        center: [1, 2],
        waypoints: [
          [1, 2],
          [3, 4],
        ],
        zoom: 12,
      }),
    ).toEqual({
      center: [1, 2],
      waypoints: [
        [1, 2],
        [3, 4],
      ],
      zoom: 12,
    });
  });

  test('it should return default if the path does not match the pattern', () => {
    expect(
      parseStateFromPathname('/some/path', {
        center: [1, 2],
        waypoints: [
          [1, 2],
          [3, 4],
        ],
        zoom: 12,
      }),
    ).toEqual({
      center: [1, 2],
      waypoints: [
        [1, 2],
        [3, 4],
      ],
      zoom: 12,
    });
  });

  test.todo('it should perform some validation');

  test('it should return the state from the pathname', () => {
    expect(
      parseStateFromPathname('/c:3,4/z:13/[[1,2],[3,4],[5,6]]', {
        center: [1, 2],
        waypoints: [
          [1, 2],
          [3, 4],
        ],
        zoom: 12,
      }),
    ).toEqual({
      center: [3, 4],
      waypoints: [
        [1, 2],
        [3, 4],
        [5, 6],
      ],
      zoom: 13,
    });
  });
});
