import { Position } from '../types';

interface State {
  center: Position;
  waypoints: Position[];
  zoom: number;
}

export const parseStateFromPathname = (
  pathname: string,
  defaults: State,
): State => {
  const match = pathname.match(/\/c:([^/]+)\/z:([^/]+)(\/([^/]+))?/);

  if (!match) {
    return defaults;
  }

  // TODO: should validate the path segments
  // to see if they respect the correct values

  const [centerLngStr, centerLatStr] = match[1].split(',');

  try {
    // TODO: should validate better

    return {
      center: [parseFloat(centerLngStr), parseFloat(centerLatStr)],
      waypoints: match[4] ? JSON.parse(match[4]) : [],
      zoom: parseFloat(match[2]),
    };
  } catch {
    return defaults;
  }
};
