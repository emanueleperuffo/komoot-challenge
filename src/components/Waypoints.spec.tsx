import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { Waypoints } from './Waypoints';

const noop = () => {};

describe('Waypoints', () => {
  it('should render the waypoints starting from 1', async () => {
    render(
      <Waypoints
        waypoints={[
          [1, 2],
          [3, 4],
        ]}
        onRemoveWaypoint={noop}
        onSortWaypoints={noop}
      />,
    );

    await expect(screen.findByText('Waypoint 0')).rejects.toThrow();
    await expect(screen.findByText('Waypoint 1')).resolves.not.toBeFalsy();
    await expect(screen.findByText('Waypoint 2')).resolves.not.toBeFalsy();
    await expect(screen.findByText('Waypoint 3')).rejects.toThrow();
  });

  it('should call remove with the right waypoint', async () => {
    const handleDelete = jest.fn();

    const user = userEvent.setup();

    render(
      <Waypoints
        waypoints={[
          [1, 2],
          [3, 4],
          [5, 6],
        ]}
        onRemoveWaypoint={handleDelete}
        onSortWaypoints={noop}
      />,
    );

    await user.click(screen.getByLabelText('Remove Waypoint 2'));

    expect(handleDelete).toHaveBeenCalledWith([3, 4]);
  });

  it.todo('should sort the endpoints with drag & drop');
});
