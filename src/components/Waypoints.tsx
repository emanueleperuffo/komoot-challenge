/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react';
import { useState } from 'react';
import { Position } from '../types';
import { sortDropWaypoints } from '../utils/sortDropWaypoints';

const waypointsCss = css({
  margin: 0,
  padding: 0,
  listStyle: 'none',
});

const waypointCss = css({
  display: 'flex',
  alignItems: 'center',
  gap: '0.5rem',
  fontWeight: 'bold',
  padding: '0.5rem 0',
  borderTop: '0.125rem solid transparent',
  borderBottom: '0.125rem solid transparent',
});

const waypointTextCss = css({
  flexGrow: 1,
});

const waypointDraggingOverBeforeCss = css({
  borderTop: '0.125rem solid #3e83e6',
});

const waypointDraggingOverAfterCss = css({
  borderBottom: '0.125rem solid #3e83e6',
});

const buttonCss = css({
  background: 'none',
  border: 'none',
  color: '#737373',
  margin: 0,
  padding: 0,
});

const dragButtonCss = css(buttonCss, {
  cursor: 'move',
});

const removeButtonCss = css(buttonCss, {
  cursor: 'pointer',
});

interface Props {
  waypoints: Position[];
  onRemoveWaypoint(waypoint: Position): void;
  onSortWaypoints(waypoints: Position[]): void;
}

export const Waypoints: React.FC<Props> = ({
  onRemoveWaypoint,
  onSortWaypoints,
  waypoints,
}) => {
  const [move, setMove] = useState<{
    index: number;
    where: 'before' | 'after';
  }>();

  return (
    <ul css={waypointsCss}>
      {waypoints.map((waypoint, index) => (
        <li
          onDragOver={(e) => {
            e.preventDefault();

            const liHeight = e.currentTarget.offsetHeight;
            const mouseY = e.nativeEvent.offsetY;

            setMove({
              index,
              where: mouseY < liHeight / 2 ? 'before' : 'after',
            });
          }}
          onDrop={(e) => {
            e.preventDefault();

            if (!move) {
              return;
            }

            const droppedIndex = parseInt(e.dataTransfer.getData('text/plain'));

            onSortWaypoints(sortDropWaypoints(waypoints, droppedIndex, move));
          }}
          key={JSON.stringify(waypoint)} // TODO: find a more optimized way to stringify
          css={[
            waypointCss,
            move?.index === index &&
              (move.where === 'before'
                ? waypointDraggingOverBeforeCss
                : waypointDraggingOverAfterCss),
          ]}
        >
          <button
            aria-label={`Sort Waypoint ${index + 1}`}
            css={dragButtonCss}
            draggable
            onDragStart={(e) => {
              e.dataTransfer.setDragImage(e.currentTarget.parentElement!, 0, 0);
              e.dataTransfer.setData('text/plain', String(index));
            }}
            onDragEnd={() => {
              setMove(undefined);
            }}
          >
            ☰
          </button>
          <div css={waypointTextCss}>Waypoint {index + 1}</div>
          <button
            aria-label={`Remove Waypoint ${index + 1}`}
            css={removeButtonCss}
            onClick={() => {
              onRemoveWaypoint(waypoint);
            }}
          >
            🗑
          </button>
        </li>
      ))}
    </ul>
  );
};
