/** @jsxImportSource @emotion/react */
// eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxCss from '!!file-loader!mapbox-gl/dist/mapbox-gl.css';
import { css } from '@emotion/react';
import React, { useEffect, useRef } from 'react';
import { Helmet } from 'react-helmet';
import { Position } from '../types';

const mapCss = css({
  height: '100%',
  width: '100%',
});

interface Props {
  center: Position;
  onClick(position: Position): void;
  onIdle(centerZoom: { center: Position; zoom: number }): void;
  waypoints: Position[];
  zoom: number;
}

const waypoints2GeoJsonLineString = (
  waypoints: Position[],
): GeoJSON.Feature<GeoJSON.LineString> => ({
  type: 'Feature',
  properties: {},
  geometry: {
    type: 'LineString',
    coordinates: waypoints,
  },
});

const waypoints2GeoJsonPoints = (
  waypoints: Position[],
): GeoJSON.FeatureCollection<GeoJSON.Point> => ({
  type: 'FeatureCollection',
  features: waypoints.map(
    (waypoint, index): GeoJSON.Feature<GeoJSON.Point> => ({
      type: 'Feature',
      properties: {
        waypoint: index + 1,
      },
      geometry: {
        type: 'Point',
        coordinates: waypoint,
      },
    }),
  ),
});

export const Map: React.FC<Props> = ({
  center,
  onClick,
  onIdle,
  waypoints,
  zoom,
}) => {
  const mapHtmlElementRef = useRef<HTMLDivElement>(null);

  const mapRef = useRef<mapboxgl.Map>();

  // Transforming props to refs because the map is created asynchronously
  // and the initialization hook (import mapbox + create map) is not re-run when changing props
  // These repetitions could have been exported to a useChangingRef hook, but at the moment
  // eslint complains if it doesn't detect returns from known hooks within the dependencies of useEffect

  const centerRef = useRef<typeof center>(center);

  useEffect(() => {
    // Skip if the arrays are equal
    if (String(centerRef.current) === String(center)) {
      return;
    }

    centerRef.current = center;
    mapRef.current?.setCenter(center);
  }, [center]);

  const onIdleRef = useRef<typeof onIdle>(onIdle);

  useEffect(() => {
    onIdleRef.current = onIdle;
  }, [onIdle]);

  const zoomRef = useRef<typeof zoom>(zoom);

  useEffect(() => {
    zoomRef.current = zoom;
    mapRef.current?.setZoom(zoom);
  }, [zoom]);

  const onClickRef = useRef<typeof onClick>(onClick);

  useEffect(() => {
    onClickRef.current = onClick;
  }, [onClick]);

  const waypointsRef = useRef<typeof waypoints>(waypoints);

  useEffect(() => {
    // Updating mapbox sources

    // Skip if the arrays are equal
    if (String(waypointsRef.current) === String(waypoints)) {
      return;
    }

    waypointsRef.current = waypoints;

    (
      mapRef.current?.getSource('waypoints-linestring') as
        | mapboxgl.GeoJSONSource
        | undefined
    )?.setData(waypoints2GeoJsonLineString(waypoints));

    (
      mapRef.current?.getSource('waypoints-points') as
        | mapboxgl.GeoJSONSource
        | undefined
    )?.setData(waypoints2GeoJsonPoints(waypoints));
  }, [waypoints]);

  useEffect(() => {
    // Removing the map on unmount

    mapRef.current?.remove();
  }, []);

  useEffect(() => {
    // Initialization hook

    let mounted = true; // Prevents initialization if the component unmounts early

    import('mapbox-gl').then(({ default: mapboxgl }) => {
      if (!mounted) {
        return;
      }

      mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN!;

      if (!mapHtmlElementRef.current) {
        return;
      }

      const map = new mapboxgl.Map({
        center: centerRef.current,
        container: mapHtmlElementRef.current,
        style: 'mapbox://styles/mapbox/outdoors-v11',
        zoom: zoomRef.current,
      });

      mapRef.current = map;

      map.on('load', () => {
        map.resize();

        map.addSource('waypoints-linestring', {
          type: 'geojson',
          data: waypoints2GeoJsonLineString(waypointsRef.current),
        });

        map.addSource('waypoints-points', {
          type: 'geojson',
          data: waypoints2GeoJsonPoints(waypointsRef.current),
        });

        map.addLayer({
          id: 'route',
          type: 'line',
          source: 'waypoints-linestring',
          paint: {
            'line-color': '#3e83e6',
            'line-width': 8,
          },
        });

        map.addLayer({
          id: 'waypoint-circles',
          type: 'circle',
          source: 'waypoints-points',
          paint: {
            'circle-color': '#3b3b3b',
            'circle-radius': 15,
          },
        });

        map.addLayer({
          id: 'waypoint-labels',
          type: 'symbol',
          source: 'waypoints-points',
          layout: {
            'text-field': '{waypoint}',
            'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
            'text-size': 16,
          },
          paint: {
            'text-color': '#fff',
            'text-color-transition': { delay: 0, duration: 0 },
            'text-opacity': 1,
            'text-opacity-transition': { delay: 0, duration: 0 },
          },
        });
      });

      map.on('click', (e) => {
        const { lat, lng } = e.lngLat;

        onClickRef.current([lng, lat]);
      });

      map.on('idle', (e) => {
        const center = map.getCenter();

        onIdleRef.current({
          center: [center.lng, center.lat],
          zoom: map.getZoom(),
        });
      });
    });

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <>
      <Helmet>
        <link rel="stylesheet" href={mapboxCss} />
      </Helmet>

      <div css={mapCss} ref={mapHtmlElementRef} />
    </>
  );
};
