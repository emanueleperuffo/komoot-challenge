/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react';
import { Position } from '../types';
import { Waypoints } from './Waypoints';

const routeBuilderCss = css({
  boxSizing: 'border-box',
  background: '#3b3b3b',
  minHeight: '100%',
  display: 'flex',
  flexDirection: 'column',
  padding: '1rem',
  gap: '0.5rem',
  color: 'white',
});

const titleCss = css({
  fontSize: '1.2rem',
  margin: '0.5rem 0 0',
});

const separatorCss = css({
  hr: {
    border: '0.125rem solid #4c4c4c',
  },
});

const waypointsCss = css({
  margin: '2rem 0 0',
  padding: '0 1rem',
  flexGrow: 1,
});

const downloadButtonCss = css({
  background: '#c5e256',
  color: '#0c0d05',
  borderRadius: '0.5rem',
  border: 'none',
  fontWeight: 'bold',
  lineHeight: '2.5rem',
  fontSize: '1rem',
  cursor: 'pointer',
});

interface Props {
  waypoints: Position[];
  onDownloadRoute(): void;
  onRemoveWaypoint(waypoint: Position): void;
  onSortWaypoints(waypoints: Position[]): void;
}

// TODO: display some blank page if the waypoints array is empty

export const RouteBuilder: React.FC<Props> = ({
  onDownloadRoute,
  onRemoveWaypoint,
  onSortWaypoints,
  waypoints,
}) => (
  <div css={routeBuilderCss}>
    <h1 css={titleCss}>Route Builder</h1>

    <div css={separatorCss}>
      <hr />
    </div>

    <div css={waypointsCss}>
      <Waypoints
        onRemoveWaypoint={onRemoveWaypoint}
        onSortWaypoints={onSortWaypoints}
        waypoints={waypoints}
      />
    </div>

    <button
      css={downloadButtonCss}
      onClick={() => {
        onDownloadRoute();
      }}
    >
      Download your Route
    </button>
  </div>
);
