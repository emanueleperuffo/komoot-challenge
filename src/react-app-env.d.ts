/// <reference types="react-scripts" />

declare module '!!file-loader!mapbox-gl/dist/mapbox-gl.css' {
  declare const css: string;

  export default css;
}
